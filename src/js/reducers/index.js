import { ADD_ARTICLE, FOUND_BAD_WORD } from "../constants/action-types";

const initialState = {
    articles: [],
    remoteArticles: []
};

function rootReducer(state = initialState, action) {
    if (action.type === ADD_ARTICLE) {
        return Object.assign({}, state, { articles: state.articles.concat(action.payload) });

        // state.articles.push(action.payload);
    }
    if (action.type === FOUND_BAD_WORD) {
        return Object.assign({}, state, { message: "You can't use this word" });
    }

    if (action.type === "DATA_LOADED") {
        console.log("state.remote", action.payload);
        return Object.assign({}, state, {
            remoteArticles: state.remoteArticles.concat(action.payload)
        });
    }
    return state;
}

export default rootReducer;
